<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Infinity Testing Material</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.php" class="logo"><b>INFINITY TESTING SERVICES</b></a>
            <!--logo end-->
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="index.php">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Mr. Gholap</h5>
              	  	
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Testing Material & Test</span>
                      </a>
                      <ul class="sub">
                          <li class="active"><a  href="material.php">Test Material</a></li>
                          <li><a  href="test_perform.php">Test Perform</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class=" fa fa-bar-chart-o"></i>
                          <span>Our Clients</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="clients_detail.php">Clients Details</a></li>
                          <li><a  href="chartjs.html">Test Report</a></li>
                          <li><a  href="chartjs.html">Invoices</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Name of Testing Materials</h3>
				
				

              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Test Material<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" style="float:right;margin-right:100px">ADD NEW</button></h4>

	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Sr. No.</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Material Name</th>
                                  <!-- <th><i class="fa fa-bookmark"></i>Material Name</th> -->
                                  <!-- <th><i class=" fa fa-edit"></i> Status</th> -->
                                  <th></th>
                              </tr>
                              </thead>
                              <div id="DeleteSuccess" style="color:green;font-weight:bold"></div>
                              <tbody id="show">
                              
                              
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <?php include ('popup/add_material.php');?>
      <?php include ('popup/edit_material.php');?>


      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              Copyright © 2017 Hives Online India Pvt. Ltd. All rights reserved.
              <a href="basic_table.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
    
  <script>
      //custom select box

        $(document).ready(function(){
      $.ajax({ 
        url: "function.php",
        context: document.body,
        data: {action: 'materialsel'},
        type: 'post',
        success: function(response){
           $('#show').html(response);
        }
      });

      $(document).on("click", ".materialDeletebtn", function () {
          
          var deleteid = $(this).data('id');

          //alert(deleteid);

        $.ajax({

            url:'function.php',
            type:'post',
            data:{action:'deleteRow',deletRowid:deleteid},
            success: function(deleteresponse)
            {
              $('#DeleteSuccess').html(deleteresponse);

                setTimeout(function(){
                location.reload(); 
                }, 500);
            }
        });
      });
      
});

      $(document).on("click", ".materialEditbtn", function () {
      var matID = $(this).data('id');
      var matName = $(this).data('name');

      $(".modal-body #matEditID").val( matID );
      $(".modal-body #matEditNamee").val( matName );

      });

      $(function(){
          $('select.styled').customSelect();
      });

    

  </script>

  </body>
</html>
