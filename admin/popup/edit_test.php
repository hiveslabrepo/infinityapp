<div class="modal fade" id="myModalTestEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Test Name</h4>
                  </div>
                  <div class="modal-body">
                    <div id="edittestsuccess" style="color:green;font-weight:bold"></div>
                    <div id="edittesterror" style="color:red;font-weight:bold"></div>
                    <input type="text" class="form-control" id="materialEditid" style="text-transform:uppercase">
                    <input type="text" class="form-control" id="testEditnamee">
                    <input type="text" class="form-control" id="EditPricenamee">
                    <input type="hidden" class="form-control" id="EdittestID">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="EdittestbtnID">Update</button>
                  </div>
                </div>
              </div>
            </div>

            <script src="assets/js/jquery.js"></script>

        <script type="text/javascript">
        $(document).ready(function() {
        $('#EdittestbtnID').click(function(){
        
        var Editmaterialname = $('#materialEditid').val();

        var Edittestname = $('#testEditnamee').val();
        var Editpricevalue = $('#EditPricenamee').val();
        var EdittestlId = $('#EdittestID').val();


        $.ajax
        ({
        url: 'function.php',
        data: {action: 'Edittest',Editmaterialname:Editmaterialname,Edittestname:Edittestname,Editpricevalue:Editpricevalue,EdittestlId:EdittestlId},
        type: 'post',
        success: function(resultdata) {

          if(resultdata == 'Please Enter Valid Material Name') 
          {
            $('#edittesterror').html(resultdata);
          }

          else {
         
        $('#edittestsuccess').html(resultdata);

        setTimeout(function(){
           location.reload(); 
      }, 3000); 
        
        }
      }
      });
});
        });

       

        </script>