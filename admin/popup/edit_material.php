<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Material Name</h4>
                  </div>
                  <div class="modal-body">
                    <div id="editsuccess" style="color:green;font-weight:bold"></div>
                    <input type="text" class="form-control" id="matEditNamee" value=""/>
                    <input type="hidden" class="form-control" id="matEditID" value=""/>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="EditbtnID">Update</button>
                  </div>
                </div>
              </div>
            </div>
            <script src="assets/js/jquery.js"></script>

        <script type="text/javascript">
        $(document).ready(function() {
        $('#EditbtnID').click(function(){
        
        var Editmaterialname = $('#matEditNamee').val();

        var EditmaterialId = $('#matEditID').val();


        $.ajax
        ({
        url: 'function.php',
        data: {action: 'Editmaterial',Editmaterialname:Editmaterialname,editmaterialID:EditmaterialId},
        type: 'post',
        success: function(resultdata) {
         
        $('#editsuccess').html(resultdata);

        setTimeout(function(){
           location.reload(); 
      }, 3000); 
        
        }
      });
});
        });

       

        </script>