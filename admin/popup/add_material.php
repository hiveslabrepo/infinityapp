<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Material Name</h4>
                  </div>
                  <div class="modal-body">
                    <div id="blankerror" style="color:red;font-weight:bold"></div>
                   <input type="text" class="form-control" id="materialNameid" placeholder="Enter Material Name" style="text-transform:uppercase" required>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savebtn">Save </button>
                  </div>
                </div>
              </div>
            </div>

      <script src="assets/js/jquery.js"></script>

        <script type="text/javascript">
        $(document).ready(function() {
        $('#savebtn').click(function(){
        
        var materialname = $('#materialNameid').val();

        if(materialname =='') 
        {
          $("#blankerror").html('Fill this field......');
        }

        else {

        $.ajax
        ({
        url: 'function.php',
        data: {action: 'material',materialname:materialname},
        type: 'post',
        success: function(data) {

        $('#show').html(data);

        setTimeout(function(){
           location.reload(); 
      }, 300);
        
        }
      });
}
        });

        });

        </script>