<div class="modal fade" id="myModalTestAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Test Name</h4>
                  </div>
                  <div class="modal-body">
                    <div class="btn-group">
                      <button type="button" class="btn btn-theme04 dropdown-toggle" data-toggle="dropdown">
                        Choose Material Name <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu" id="choosematerialid">
                        <?php
                        include ('config.php');
                      $sql2 = "SELECT id,name FROM material";
                      $result = mysqli_query($conn, $sql2);

                      if (mysqli_num_rows($result) > 0) {

                      while($row = mysqli_fetch_assoc($result)) {?>
                        <li><a href="#"><?php echo $row['name'];?></a></li>
                        <?php } }?>
                        
                      </ul>
                    </div>
                    <div id="testblankerror" style="color:red;font-weight:bold"></div>
                   <input type="text" class="form-control" id="testNameid" placeholder="Enter Test Name" required>
                   <input type="text" class="form-control" id="testPriceid" placeholder="Enter Test Price" required>
                   <input type="hidden" class="form-control" id="MatId" placeholder="Enter Test Price" required>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savetestbtn">Save</button>
                  </div>
                </div>
              </div>
            </div>
    <script src="assets/js/jquery.js"></script>
    <script type="text/javascript">
     $(document).ready(function() {
      var selText;
        $(".dropdown-menu li a").click(function(){
        selText = $(this).text();
        
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
        });
         
            $('#savetestbtn').click(function(){
            
            var testname = $('#testNameid').val();
            var testprice = $('#testPriceid').val();
            
            if(testname =='' || testprice =='') 
            {
              $("#testblankerror").html('Please Fill all the field......');
            }

            else {

            $.ajax
            ({
            url: 'function.php',
            data: {action: 'test',testname:testname,testprice:testprice,selTextli:selText},
            type: 'post',
            success: function(data) {

            $('#showResult').html(data);


            setTimeout(function(){
               location.reload(); 
          }, 300);
            
            }
          });
    }
            });

            });


    </script>