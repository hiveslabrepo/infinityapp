<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.col-lg-4 {
    margin-left: 15%;
}
/*@import "compass/css3";*/

.table-editable {
  position: relative;
  
  .glyphicon {
    font-size: 20px;
  }
}

.table-remove {
  color: #700;
  cursor: pointer;
  
  &:hover {
    color: #f00;
  }
}

.table-up, .table-down {
  color: #007;
  cursor: pointer;
  
  &:hover {
    color: #00f;
  }
}

.table-add {
  color: #070;
  cursor: pointer;
  position: absolute;
  top: 8px;
  right: 0;
  
  &:hover {
    color: #0b0;
  }
  td:last-child {

    border:none;
  }
}

@media (min-width: 1200px){
.col-lg-4 {
    width: 33.33333333%;
     margin-left: 15%;
}
}


</style>
</head>
<body>
  <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-8 col-lg-2"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
      <h1 style="text-align:center;">INFINITY</h1>
      <h4 style="text-align:center;">TESTING SERVICES</h4>
      <h6 style="text-align:center;">(An ISO 9001 : 2015 Certified Company)</h6>
      <h2 style="text-align:center;">Test Report</h2><br>
       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <p><strong>REPORT NO.:-</strong> Infin/Jan-22/br/16-17</p>
        <p><strong>DATE OF RECEIPT:-</strong> 6-Jan-17</p>
        <p><strong>CUSTOMER NAME & ADDRESS:-</strong> Dnyanjyot Construction</p>
        <p><strong>SITE ADDRESS:-</strong> Ward No.2</p>7,Construction of Toilet work under Swachcha Bharat Abhiyan at Kothrud Ward Office
        <p><strong>DISCRIPTION:-</strong> Material-Flyash Bricks <br><strong>Grade-</strong> ---</p>
        <p><strong>QUANTITY:-</strong> 10 nos</p>


       </div>

        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

          <p><strong>REPORT DATE.:-</strong> 12/01/2017</p>
          <p><strong>DATE OF TESTING:STING-</strong> 11/01/2017</p>
          <p><strong>DATE OF CASTING-</strong>-----</p>
           <p><strong>AGE(in days)-</strong>-----</p>
        </div>


  </div>
  <div class="col-xs-2 col-sm-2 col-md-8 col-lg-2"></div>
</div>
  <div class="row">
    <div class="col-md-2">
</div>
<div class="col-md-8">

   <h3 style="text-align:center;">A. Water Absorption</h3>
  <span class="table-add glyphicon glyphicon-plus"></span>
   
   <div id="table" class="table-editable">
  <!--   <span class="table-add glyphicon glyphicon-plus"></span> -->
    <table class="table table-bordered">
      <tr>
        <th>Sr.No</th>
        <th>Size Of bricks(mm)</th>
        <th>Oven dry weight of bricks in(Kg)W2</th>
        <th>Weight of saturated bricks in(Kg)W1</th>
        <th>Percentage of water absorption(%)=(w1-w2/w2)*100</th>
        <th>Specification of limit</th>
      </tr>
      <tr>
        <td contenteditable="true">1</td>
        <td contenteditable="true">229*151*75</td>
        <td contenteditable="true">5.092</td>
        <td contenteditable="true">5.555</td>
        <td contenteditable="true">9.09</td>
         <td contenteditable="true">IS 1077-1992 up tp 20 class 12.5 and 15 for higher class</td>
      </tr>
      <!-- This is our clonable table line -->
      <tr class="hide">
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
      </tr>
    </table>
  </div>
  
  
 <!--  <button id="export-btn" class="btn btn-primary">Export Data</button>
  <p id="export"></p> -->
</div>
<div class="col-md-2">
</div>
</div>

<script>
var $TABLE = $('#table');
var $BTN = $('#export-btn');
var $EXPORT = $('#export');

$('.table-add').click(function () {
  var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
  $TABLE.find('.table').append($clone);
});





// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.click(function () {
  var $rows = $TABLE.find('tr:not(:hidden)');
  var headers = [];
  var data = [];
  
  // Get the headers (add special header logic here)
  $($rows.shift()).find('th:not(:empty)').each(function () {
    headers.push($(this).text().toLowerCase());
  });
  
  // Turn all existing rows into a loopable array
  $rows.each(function () {
    var $td = $(this).find('td');
    var h = {};
    
    // Use the headers from earlier to name our hash keys
    headers.forEach(function (header, i) {
      h[header] = $td.eq(i).text();   
    });
    
    data.push(h);
  });
  
  // Output the result
  $EXPORT.text(JSON.stringify(data));
});

</script>

<!-- *****************************tabel 1 end************** -->

 <div class="row">
    <div class="col-md-2">
</div>
<div class="col-md-8">

   <h3 style="text-align:center;">B. Compressive Strength</h3>
  
 <span class="table-add1 glyphicon glyphicon-plus" style="float:right;color:green;"></span>
 <br>
   
   <div id="table1" class="table-editable">
  <!--   <span class="table-add glyphicon glyphicon-plus"></span> -->
    <table class="table1 table-bordered">
      <tr>
        <th>Sr.No</th>
        <th>I.D.Mark</th>
        <th>Size of bricks(mm)</th>
        <th>Area of bricks(mm2)</th>
        <th>Crushing Load(KN)</th>
        <th  colspan="3">Compressive Strength</th>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>N/mm2</td>
         <td>Kg/cm2</td>
          <td>Specification of limit</td>
      </tr>

       <tr>
        <td contenteditable="true">1</td>
        <td contenteditable="true"></td>
        <td contenteditable="true">231*150*76</td>
        <td contenteditable="true">34650</td>
        <td contenteditable="true">342.6</td>
         <td contenteditable="true">9.89</td>
          <td contenteditable="true">98.9</td>
           <td contenteditable="true">As per Table 1,4.1 of IS 1077 1992</td>
      </tr>
      <!-- This is our clonable table line -->
      <tr class="hide1">
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
      </tr>
    </table>
    <p><strong>Note:-</strong> 1.Any test report shall not be reproduced without written permission from INFINITY TESTING SERVICES.<br>
             2.Result related only to the samples under test in as received condition and applicable parameter.</p>
  </div>
  
 <!--  <button id="export-btn" class="btn btn-primary">Export Data</button>
  <p id="export"></p> -->
</div>
<div class="col-md-2">
  <!-- <span class="table-add1 glyphicon glyphicon-plus" style="color:green;"></span> -->
</div>

</div>
<div class="container">
  <div class="jumbotron" style="background-color:black;height:80px;padding-top:0px;">
    <div class="row">
      <div class="col-md-4">
        <h5 style="color:white;"><span class="glyphicon glyphicon-map-marker"></span>Registered Office</h4>
        <p style="font-size:15px;color:white;">204,Vassanti,Survey No. 13/1,Ganesh Nagar,Dhayari,Pune</p>

    </div>

    <div class="col-md-4">
      <h5 style="color:white;"><span class="glyphicon glyphicon-map-marker"></span>Lab Office</h5>
        <p style="font-size:15px;color:white;">Sr.No. 32/1 A,In Front Of Katraj</p>

    </div>

    <div class="col-md-4">
      <h5 style="color:white;"><span class="glyphicon glyphicon-map-marker"></span>Contact</h5>
        <p style="font-size:15px;color:white;">(020)65000534<br>lab@ceinfinity.com</p>

    </div>
    
 
  </div>
</div>
</div>

<script>
var $TABLE1 = $('#table1');
$('.table-add1').click(function () { 
  var $clone1 = $TABLE1.find('tr.hide1').clone(true).removeClass('hide1 table-line');
  $TABLE1.find('.table1').append($clone1);
});





// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.click(function () {
  var $rows = $TABLE.find('tr:not(:hidden)');
  var headers = [];
  var data = [];
  
  // Get the headers (add special header logic here)
  $($rows.shift()).find('th:not(:empty)').each(function () {
    headers.push($(this).text().toLowerCase());
  });
  
  // Turn all existing rows into a loopable array
  $rows.each(function () {
    var $td = $(this).find('td');
    var h = {};
    
    // Use the headers from earlier to name our hash keys
    headers.forEach(function (header, i) {
      h[header] = $td.eq(i).text();   
    });
    
    data.push(h);
  });
  
  // Output the result
  $EXPORT.text(JSON.stringify(data));
});

</script>

	</body>
	</html>