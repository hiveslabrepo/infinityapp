<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <link rel="shortcut icon" href="admin/assets/img/tlogo.png">
  <title>Infinity Testing Services-Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="admin/assets/js/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
  <style>
  body {
      font: 400 15px Lato, sans-serif;
      line-height: 1.8;
      color: #818181;
  }
  fieldset { border:1px solid green ;}
  h2 {
      font-size: 24px;
      text-transform: uppercase;
      color: #303030;
      font-weight: 600;
      margin-bottom: 30px;
  }
  h4 {
      font-size: 19px;
      line-height: 1.375em;
      color: #303030;
      font-weight: 400;
      margin-bottom: 30px;
  }  

  .hide1 {
    display: none!important;
}


  .col-lg-4 {
    margin-left: 15%;
}
@import "compass/css3";

.table-editable {
  position: relative;
  
  .glyphicon {
    font-size: 20px;
  }
}

.table-remove {
  color: #700;
  cursor: pointer;
  
  &:hover {
    color: #f00;
  }
}

.table-up, .table-down {
  color: #007;
  cursor: pointer;
  
  &:hover {
    color: #00f;
  }
}

.table-add {
  color: #070;
  cursor: pointer;
  position: absolute;
  top: 8px;
  right: 0;
  
  &:hover {
    color: #0b0;
  }
  td:last-child {

    border:none;
  }
}

@media (min-width: 1200px){
.col-lg-4 {
    width: 33.33333333%;
     margin-left: 15%;
}
}
  .jumbotron {
      background-color: #f4511e;
      color: #fff;
      padding: 100px 25px;
      font-family: Montserrat, sans-serif;
  }
  .container-fluid {
      padding: 60px 50px;
  }
  .bg-grey {
      background-color: #f6f6f6;
  }
  .logo-small {
      color: #f4511e;
      font-size: 50px;
  }
  .logo {
      color: #f4511e;
      font-size: 200px;
  }
  .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
  }
  .thumbnail img {
      width: 100%;
      height: 100%;
      margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
      background-image: none;
      color: #f4511e;
  }
  .carousel-indicators li {
      border-color: #f4511e;
  }
  .carousel-indicators li.active {
      background-color: #f4511e;
  }
  .item h4 {
      font-size: 19px;
      line-height: 1.375em;
      font-weight: 400;
      font-style: italic;
      margin: 70px 0;
  }
  .item span {
      font-style: normal;
  }
  .panel {
      border: 1px solid #f4511e; 
      border-radius:0 !important;
      transition: box-shadow 0.5s;
  }
  .panel:hover {
      box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
      border: 1px solid #f4511e;
      background-color: #fff !important;
      color: #f4511e;
  }
  .panel-heading {
      color: #fff !important;
      background-color: #f4511e !important;
      padding: 25px;
      border-bottom: 1px solid transparent;
      border-top-left-radius: 0px;
      border-top-right-radius: 0px;
      border-bottom-left-radius: 0px;
      border-bottom-right-radius: 0px;
  }
  .panel-footer {
      background-color: white !important;
  }
  .panel-footer h3 {
      font-size: 32px;
  }
  .panel-footer h4 {
      color: #aaa;
      font-size: 14px;
  }
  .panel-footer .btn {
      margin: 15px 0;
      background-color: #f4511e;
      color: #fff;
  }
  .navbar {
      margin-bottom: 0;
      background-color: #f4511e;
      z-index: 9999;
      border: 0;
      font-size: 12px !important;
      line-height: 1.42857143 !important;
      letter-spacing: 4px;
      border-radius: 0;
      font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
      color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
      color: #f4511e !important;
      background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
  }
  footer .glyphicon {
      font-size: 20px;
      margin-bottom: 20px;
      color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
      animation-name: slide;
      -webkit-animation-name: slide;
      animation-duration: 1s;
      -webkit-animation-duration: 1s;
      visibility: visible;
  }
  .dropdown-menu {
    margin-left: 450px;
  }

  .checkboxes {
    columns: 2 8em;

}

.checkboxes li {
    border: 1px solid #CCC;
    padding-left:120px;
}
ul {
  list-style-type: none;
}
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
        width: 100%;
        margin-bottom: 35px;
    }
    .dropdown-menu {
    margin-left: 0px;
  }

  .checkboxes li {
    border: 1px solid #CCC;
    padding-left:0px;
}
  @media screen and (max-width: 480px) {
    .logo {
        font-size: 150px;
    }
  }

/*style for tables*/

  
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="home.php" style="margin-bottom:30px"><img src="admin/assets/img/infinity.png" width="250px;" height="70px;"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <!-- <li><a href="#about">ABOUT</a></li>
        <li><a href="#services">SERVICES</a></li>
        <li><a href="#portfolio">PORTFOLIO</a></li>
        <li><a href="#pricing">PRICING</a></li>
        <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center">
  <h1>INFINITY TESTING SERVICES</h1> 
  <p>We Specialize in Material Testing</p> 
  <form>
    <div class="input-group">
      <!-- <input type="email" class="form-control" size="50" placeholder="Email Address" required>
      <div class="input-group-btn">
        <button type="button" class="btn btn-danger">Subscribe</button> -->
      </div>
    </div>
  </form>
</div>


<fieldset id="print">
<!-- Container (Services Section) -->

<div id="showfullreport" style="display:none">
<div class="row">
    <div class="col-xs-2 col-sm-2 col-md-8 col-lg-2"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
      <h1 style="text-align:center;"> <img src="images/logo.jpg" style="width: 5%;">INFINITY</h1>
      <h4 style="text-align:center;">TESTING SERVICES</h4>
      <h6 style="text-align:center;">(An ISO 9001 : 2015 Certified Company)</h6>
      <h2 style="text-align:center;">Test Report</h2><br>
       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <p><strong>REPORT NO.:-</strong><small contenteditable="true"> Infin/Jan-22/br/16-17</small></p>
        <p><strong>DATE OF RECEIPT:-</strong><small contenteditable="true"> 6-Jan-17</small></p>
        <p><strong>CUSTOMER NAME & ADDRESS:-</strong><small contenteditable="true"> Dnyanjyot Construction</small></p>
        <p><strong>SITE ADDRESS:-</strong><small contenteditable="true"> Ward No.27,Construction of Toilet work under Swachcha Bharat Abhiyan at Kothrud Ward Office</small></p>
        <p><strong>DISCRIPTION:-</strong><small contenteditable="true"> Material-Flyash Bricks</small> <br><strong>Grade-</strong><small contentsditable="true"> ---</small></p>
        <p><strong>QUANTITY:-</strong> <small contenteditable="true">10 nos</small></p>


       </div>

        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

          <p><strong>REPORT DATE.:-</strong> <small contenteditable="true">12/01/2017</small></p>
          <p><strong>DATE OF TESTING:STING-</strong> <small contenteditable="true">11/01/2017</small></p>
          <p><strong>DATE OF CASTING-</strong><small contenteditable="true">-----</small></p>
           <p><strong>AGE(in days)-</strong><small contenteditable="true">-----</small></p>
        </div>


  </div>
  <div class="col-xs-2 col-sm-2 col-md-8 col-lg-2"></div>
</div>
</div>
<div id="hidesegment">
<div id="services" class="container-fluid text-center">
  <h2>Choose Test Material Name </h2>
  <!-- <h4>What we offer</h4> -->
  <br>
  <div class="dropdown" style="margin-top:-20px">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Choose Material Name
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
  <?php
  include('config.php');
 $sql = "SELECT id,name FROM material";

  $result = mysqli_query($conn,$sql);
  if(mysqli_num_rows($result)>0) 
  {
    while($row = mysqli_fetch_assoc($result)) 
    {?>
      <li id="<?php echo $row['id'];?>"><a href="#"><?php echo $row['name'];?></a></li>
   <?php 
    }
  }
  else {
    echo "no data found";
  }
  ?>
  </ul>
</div>
  
  
</div>

<div id="services" class="container-fluid" style="margin-top:-70px">
  <h2 class="text-center">Choose Perfoming Test Name </h2>
  <!-- <h4>What we offer</h4> -->
  <ul class="checkboxes" id="showtest">
    
    
    
</ul>
</div>
  
  
</div>

</div> 

<!-- Container (Test Readinds) -->
<div id="pricing" class="container-fluid" style="margin-top:-90px">
  <div class="text-center" id="hidehed">
    <h2>Set Readings for Test</h2>
    <h4 class="showtestname"></h4>
  </div>
  <div>

    <div class="col-md-8" style="margin-left:200px;" id="hideforinvoice">

   <h3 style="text-align:center;">A. Water Absorption</h3>
  <span class="table-add glyphicon glyphicon-plus"></span>
   
   <div id="table" class="table-editable">
  <!--   <span class="table-add glyphicon glyphicon-plus"></span> -->
    <table class="table2 table-bordered">
      <tr>
        <th>Sr.No</th>
        <th>Size Of bricks(mm)</th>
        <th>Oven dry weight of bricks in(Kg)W2</th>
        <th>Weight of saturated bricks in(Kg)W1</th>
        <th>Percentage of water absorption(%)=(w1-w2/w2)*100</th>
        <th>Specification of limit</th>
      </tr>
      <tr>
        <td contenteditable="true">1</td>
        <td contenteditable="true">229*151*75</td>
        <td contenteditable="true" id="prv2" class="year">5.092</td>
        <td contenteditable="true" id="prv" class="year2">5.555</td>
        <td contenteditable="true" class="lasttd" id="totaladd">9.09</td>
         <td contenteditable="true" rowspan="20">IS 1077-1992 up tp 20 class 12.5 and 15 for higher class</td>
         <!-- <td class="hidedeleterow"><a class="deleteRow"> x </a></td> -->
      </tr>
      
    
      <!-- This is our clonable table line -->
      <tr class="hide">
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true" id="prv2" class="year"></td>
        <td contenteditable="true" id="prv" class="year2"></td>
        <td contenteditable="true" id="totaladd" class="lasttd"></td>
        <td class="hidedeleterow"><a class="deleteRow"> x </a></td>
       <!--  <td contenteditable="true"></td> -->
      </tr>
       <tr>
        <td contenteditable="true" colspan="4">Average Water absorption</td>
        <td contenteditable="true" colspan="" id="avg"></td>
      </tr>
    </table>

    <div class="row">
    <div class="col-md-2">
</div>
<h3 style="text-align:center;">B. Compressive Strength</h3>
 <span class="table-add1 glyphicon glyphicon-plus" style="float:right;color:green;"></span>
 <br>
 <div id="table1">
    <table class="table1 table-bordered">
      <tr>
        <th>Sr.No</th>
        <th>I.D.Mark</th>
        <th colspan="3">Size of bricks(mm)</th>
        <th>Area of bricks(mm2)</th>
        <th>Crushing Load(KN)</th>
        <th  colspan="3">Compressive Strength</th>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Length(L)</td>
        <td>Width(W)</td>
        <td>Height(H)</td>
        <td>LxW</td>
        <td></td>
        <td>N/mm2</td>
         <td>Kg/cm2</td>
          <td>Specification of limit</td>
      </tr>

       <tr  class="b">
        <td contenteditable="true">1</td>
        <td contenteditable="true"></td>
        <td contenteditable="true" id="length">230</td>
        <td contenteditable="true" id="width">150</td>
        <td contenteditable="true" id="height">75</td>
        <td contenteditable="true" id="lengthwidth">34500</td>
        <td contenteditable="true" id="crushingload">143.2</td>
        <td contenteditable="true" id="Nmm" class="Nmmavg">4.15</td>
        <td contenteditable="true" id="kgcmm" class="Kgcmmavg">41.51</td>
        <td contenteditable="true" rowspan="20">As per Table 1,4.1 of IS 1077 1992</td>
        <!-- <td><a class="deleteRow"> x </a></td> -->
      </tr>

      <!-- This is our clonable table line -->
      <tr class="hide1">
        <td contenteditable="true">1</td>
        <td contenteditable="true"></td>
        <td contenteditable="true" id="length"></td>
        <td contenteditable="true" id="width"></td>
        <td contenteditable="true" id="height"></td>
        <td contenteditable="true" id="lengthwidth"></td>
        <td contenteditable="true" id="crushingload"></td>
        <td contenteditable="true" id="Nmm" class="Nmmavg"></td>
        <td contenteditable="true" id="kgcmm" class="Kgcmmavg"></td>
        <td class="hidedeleterow"><a class="deleteRow"> x </a></td>
       <!--  <td contenteditable="true"></td>
        <td contenteditable="true"></td> -->
      </tr>
       <tr>
        <td contenteditable="true" colspan="5">AverageCompressive Strength</td>
         <td contenteditable="true"></td>
         <td contenteditable="true"></td>
         <td contenteditable="true" id="avgNmm"></td>
         <td contenteditable="true" id="avgKgcmm"></td>
      </tr>
    </table>
    </div>

    <div id="info" style="display:none">
    <p><strong>Note:-</strong> 1.Any test report shall not be reproduced without written permission from INFINITY TESTING SERVICES.<br>
             2.Result related only to the samples under test in as received condition and applicable parameter.</p>
      </div>
    <!--   <div id="footershow" style="margin-left:-170px;display:none">
   <div class="container">
  <div class="jumbotron" style="background-color:black;height:80px;padding-top:0px;">
    <div class="row">
      <div class="col-md-4">
        <h5 style="color:white;"><span class="glyphicon glyphicon-map-marker"></span>Registered Office</h4>
        <p style="font-size:15px;color:white;">204,Vassanti,Survey No. 13/1,Ganesh Nagar,Dhayari,Pune</p>

    </div>

    <div class="col-md-4">
      <h5 style="color:white;"><span class="glyphicon glyphicon-map-marker"></span>Lab Office</h5>
        <p style="font-size:15px;color:white;">Sr.No. 32/1 A,In Front Of Katraj</p>

    </div>

    <div class="col-md-4">
      <h5 style="color:white;"><span class="glyphicon glyphicon-map-marker"></span>Contact</h5>
        <p style="font-size:15px;color:white;">(020)65000534<br>lab@ceinfinity.com</p>

    </div>
    
 
  </div>
</div>
</div>
    </div>
  </div> -->
  
  <div id="hidebtn">
  <button type="button" class="btn btn-primary" id ="gntreport" style="margin-left:300px;margin-top:30px">Click to Generate Test Report</button>
  </div>
  <div id="shwSave" style="display:none">
  <button type="button" class="btn btn-success" style="margin-left:200px;margin-bottom:-55px"><a style="color:white">Save Report</a></button>
  </div>
  <div id="shwprint" style="display:none">
  <button type="button" class="btn btn-success" style="margin-left:350px;"><a href="javascript:void(0);" id="printButton" style="color:white">Print</a></button>
  </div>

  <div id="shwinvoice" style="display:none">
  <button type="button" class="btn btn-success" style="margin-left:450px;margin-top: -65px"><a style="color:white" id="invoicebtn">Generate Invoice</a></button>
  </div>
 <!--  <button id="export-btn" class="btn btn-primary">Export Data</button>
  <p id="export"></p> -->
</div>
        
           
      
  </div>
</div>

<!--INVOICE CODING START-->

<div id="showinvoice" style="display:none;margin-top:30px">

<div class="row">
  <div class="col-sm-3"></div>
  <div class="col-sm-3" style="margin-top:20px;">
  <h4>INFINITY TESTING SERVICES</h4>
  <h6>Construction Material Testing Service</h6>
  </div>
  <div class="col-sm-3" style="width: 13%;">
    <h1>INFINITY</h1>
    <p>TESTING SERVICES</p>
</div>

   <div class="col-sm-3">
     <div class="verticalLine">
 <img src="images/logo.jpg" style="width: 50%;">
</div>

  </div>
</div>
<!-- *******************row1 end*************
 -->
 <hr>
 <div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <p><strong>Registered Office:-</strong>204,Vassanti,Survey No.13/1,Ganesh Nagar,Dhayari,Pune-411041</p>
    <p><strong>Tel.:-</strong>020-65000534</p>
    <p><strong>Laboratory:-</strong>Sr. No. 13/1 A Shelar Mala,in front of katraj lake,katraj,Pune-411046</p>
    <p><strong>Tel.:-</strong>020-65000534</p>
    <p><strong>E.mail :-</strong> lab@ceinfinity.com</p>
  
  </div>
  <div class="col-md-2"></div>
 
</div>

<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
     <span class="table-add glyphicon glyphicon-plus" style="float:right;color:green;"></span>
   <div id="printinvoiceT">
   <div id="table" class="table-editable">
  <!--   <span class="table-add glyphicon glyphicon-plus"></span> -->
    <table class="table table-bordered">

<!-- ***********************row2 end************** -->
<!-- <table class="table-editable" style="width:100%" >  -->
<h2 style="text-align:center;">INVOICE</h2> 
  <tr>  
    <th colspan="3">InVoice No:1174</th>  
    <th colspan="4">InVoice Date:13/01/2017</th>  
  </tr>
  <tr>  
    <th colspan="3">Client Name:Dnyanjyot Construction</th>  
    <th colspan="4">Project/Site:Kothrud Ward Office</th>  
  </tr>
   <tr>  
    <th colspan="3">Client Ref:</th>  
    <th colspan="4">Laboratory Inward No.:Infin/Jan-22/OT/16-17</th>  
  </tr>  
  <tr>  
    <th>Sr.No</th>  
    <th>Name Of Material</th>  
    <th>Name Of Test</th> 
     

     <th>Sample ID</th>  
    <th>Quantity</th>  
    <th>Rate(Rs.)</th>
    <th>Total(Rs.)</th> 

  </tr> 
  <tr>
    <td contenteditable="true">1</td>
    <td contenteditable="true" id="invoicematerial">Crushed Sand</td>
    <td contenteditable="true" id="testinvoicename">Sieve Analysis</td>

    <td contenteditable="true">----</td>
    <td contenteditable="true">1</td>
    <td contenteditable="true">325.00</td>
    <td contenteditable="true">325.00</td>

  </tr>
   <tr>
    <td contenteditable="true">2</td>
    <td contenteditable="true">Crushed Sand</td>
    <td contenteditable="true">Sieve Analysis</td>

    <td contenteditable="true">----</td>
    <td contenteditable="true">1</td>
    <td contenteditable="true">325.00</td>
    <td contenteditable="true">325.00</td>

  </tr>
   <tr>
    <td contenteditable="true">3</td>
    <td contenteditable="true"> Crushed Sand</td>
    <td contenteditable="true">Sieve Analysis</td>

    <td contenteditable="true">----</td>
    <td contenteditable="true">1</td>
    <td contenteditable="true">325.00</td>
    <td contenteditable="true">325.00</td>

  </tr>
  
   <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

    <td contenteditable="true">Sub Total</td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true">4660.00</td>

  </tr>

   <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

    <td contenteditable="true">Service Tax @ 14%</td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true">652.40</td>

  </tr>

  <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

    <td contenteditable="true">Swachh Bharat Cess @ 0.5%</td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true">23.3</td>

  </tr>
  <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

    <td contenteditable="true">Swachh Bharat Cess @ 0.5%</td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true">23.3</td>

  </tr>

  <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

    <td contenteditable="true">Total Amount payable</td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true">5359.00</td>

  </tr>

   <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

    <td contenteditable="true">Rs.Five Thousands And Three Hundred Fiftty Nine Only.</td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

  </tr>

  <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true">Income Tax PAN : AEQPT3430H</td>
    <td contenteditable="true"></td>

    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

  </tr>
  <tr>
    <td contenteditable="true"></td>
    <td contenteditable="true">Service tax No : AEQPT3430HSD001</td>
    <td contenteditable="true"></td>

    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>
    <td contenteditable="true"></td>

  </tr>
   <tr class="hide">
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
      </tr>

</table> 
</div>
</div>
<div class="col-md-2"></div>
</div> 
<div id="shwSave">
  <button type="button" class="btn btn-success" style="margin-left:500px;margin-bottom:-55px"><a style="color:white">Save Invoice</a></button>
  </div>
  <div id="shwprint">
  <button type="button" class="btn btn-success" style="margin-left:700px;"><a href="javascript:void(0);" id="printInvoice" style="color:white">Print-Invoice</a></button>
  </div>
<div class="col-md-2"></div>
</div> 
</div>
</fieldset>


<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">CONTACT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
      <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
      <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
    </div>
    <div class="col-sm-7 slideanim">
      <div class="row">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit">Send</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <div id="googleMap" style="height:400px;width:100%;"></div> -->

<!-- Add Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
var myCenter = new google.maps.LatLng(41.878114, -87.629798);

function initialize() {
var mapProp = {
  center:myCenter,
  zoom:12,
  scrollwheel:false,
  draggable:false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker = new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>Copyright © 2017 Hives Online India Pvt. Ltd. All rights reserved.</a></p>
</footer>

<script>
$(document).ready(function(){

  $('#gntreport').click(function() {
  $('#hidesegment').hide();
  $('#hidehed').hide();
  $('#hidebtn').hide();
  $('.glyphicon-plus').hide();
  $('#showfullreport').show();
  $('#shwprint').show();
  $('#shwinvoice').show();
  $('#shwSave').show();
  $('#info').show();
  $('#footershow').show();
  $('.hidedeleterow').hide();
});
  $("#printButton").click(function(){
        var mode = 'iframe'; //popup
        var close = mode == "popup";
        var options = { mode : mode, popClose : close};
        $("#print").printArea( options );
    });

  $("#printInvoice").click(function(){
        var mode = 'iframe'; //popup
        var close = mode == "popup";
        var options = { mode : mode, popClose : close};
        $("#printinvoiceT").printArea( options );
    });

  $("#invoicebtn").click(function() {

    $('#showfullreport').hide();
    $('#hideforinvoice').hide();

    $('#showinvoice').show();

  });

  $("table.table-bordered").on("keyup", '#prv2, #prv', function (event) {


        calculateRow($(this).closest("tr"));
        //calculateGrandTotal();
        
    });

  $(".table1.table-bordered").on("keyup", '#length, #width', function (event) {

          //alert('hii');
        calculateLengthWidth($(this).closest("tr"));
        //calculateGrandTotal();
        
    });

  $(".table1.table-bordered").on("keyup", '#crushingload', function (event) {

          //alert('hii');
        calculateNmm($(this).closest("tr"));
        //calculateGrandTotal();
        avgNmm();
        Kgcmm();
        
    });

  $("table.table-bordered").on("click", "a.deleteRow", function (event) {
        $(this).closest("tr").remove();
        //calculateGrandTotal();
        colSum();
        Kgcmm();
        avgNmm();
    });

    function calculateRow(row) {



      var w2 = +row.find('#prv2').text();
       
      var w1 = +row.find('#prv').text();
     // alert(qty);
      row.find('#totaladd').text((((w1-w2)/w2)*100).toFixed(2));
      colSum();
  }


  function calculateLengthWidth(row) {



      var length = +row.find('#length').text();
       
      var width = +row.find('#width').text();
      //alert(length*width);
      row.find('#lengthwidth').text((length*width).toFixed(2));
      //colSum();
  }

  function calculateNmm(row) {



      var crushingload = +row.find('#crushingload').text();
       
      var areabrics = +row.find('#lengthwidth').text();
      //alert((crushingload/areabrics)*100);
      row.find('#Nmm').text(((crushingload/areabrics)*1000).toFixed(2));
      row.find('#kgcmm').text((((crushingload/areabrics)*1000)*10).toFixed(2));
      //colSum();
  }

      

        function colSum() {
           var sum = 0;
           var count = 0;
    // iterate through each td based on class and add the values
    $(".lasttd").each(function() {

        var value = $(this).text();
        // add only if the value is number
        if(!isNaN(value) && value.length != 0) {
            sum += parseFloat(value);

            count++;
        }
    });
    $('#avg').text(sum/count);
    }


    function avgNmm() {
           var sum = 0;
           var count = 0;
    // iterate through each td based on class and add the values
    $(".Nmmavg").each(function() {

        var value = $(this).text();
        // add only if the value is number
        if(!isNaN(value) && value.length != 0) {
            sum += parseFloat(value);

            count++;
        }
    });
    $('#avgNmm').text(sum/count);
    }

    function Kgcmm() {
           var sum = 0;
           var count = 0;
    // iterate through each td based on class and add the values
    $(".Kgcmmavg").each(function() {

        var value = $(this).text();
        // add only if the value is number
        if(!isNaN(value) && value.length != 0) {
            sum += parseFloat(value);

            count++;
        }
    });
    $('#avgKgcmm').text(sum/count);
    }

    
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });


        
});

$(document).ready(function(){

  function moveValue()
{
var y = $("#test").html();
alert(y);
} 


  $(".checkboxes").click(function(){
            var favorite = [];
            $.each($("input[name='chk']:checked"), function(){            
                favorite.push($(this).val());
            });testinvoicename
            
            var test = favorite.join(", ");
            $('.showtestname').text(test);
            $('#testinvoicename').text(test);
        });

  $(".dropdown-menu li a").click(function(){
        var selText = $(this).text();
        $('#invoicematerial').text(selText);
        $(this).parents('.dropdown').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');

         $.ajax
            ({
            url: 'admin/function.php',
            data: {action: 'dependtest',selTextli:selText},
            type: 'post',
            success: function(data) {



            $('#showtest').html(data);

            }
          });
        });
    
        var $TABLE = $('#table');
        var $BTN = $('#export-btn');
        var $EXPORT = $('#export');

        $('.table-add').click(function () {
          var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
          //$TABLE.find('.table').prepend($clone);
          $('.table2 tr:last').before($clone);
        });





// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.click(function () {
  var $rows = $TABLE.find('tr:not(:hidden)');
  var headers = [];
  var data = [];
  
  // Get the headers (add special header logic here)
  $($rows.shift()).find('th:not(:empty)').each(function () {
    headers.push($(this).text().toLowerCase());
  });
  
  // Turn all existing rows into a loopable array
  $rows.each(function () {
    var $td = $(this).find('td');
    var h = {};
    
    // Use the headers from earlier to name our hash keys
    headers.forEach(function (header, i) {
      h[header] = $td.eq(i).text();   
    });
    
    data.push(h);
  });
  
  // Output the result
  $EXPORT.text(JSON.stringify(data));
});
  });


var $TABLE1 = $('#table1');
$('.table-add1').click(function () {
  var $clone1 = $TABLE1.find('tr.hide1').clone(true).removeClass('hide1 table-line');
  //$TABLE.find('.table').prepend($clone);
  $('.table1 tr:last').before($clone1);
});





// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.click(function () {
  var $rows = $TABLE.find('tr:not(:hidden)');
  var headers = [];
  var data = [];
  
  // Get the headers (add special header logic here)
  $($rows.shift()).find('th:not(:empty)').each(function () {
    headers.push($(this).text().toLowerCase());
  });
  
  // Turn all existing rows into a loopable array
  $rows.each(function () {
    var $td = $(this).find('td');
    var h = {};
    
    // Use the headers from earlier to name our hash keys
    headers.forEach(function (header, i) {
      h[header] = $td.eq(i).text();   
    });
    
    data.push(h);
  });
  
  // Output the result
  $EXPORT.text(JSON.stringify(data));
});

</script>

</body>
</html>
